import java.util.Scanner;
import java.io.*;
import java.awt.* ; 
import java.awt.event.*;
import java.util.Random;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.util.*;


class countdown
{       

    JFrame      screen;
    JPanel      mainpanel, letterpanel, buttonpanel, bottompanel, blankpanel, scorepanel, randompanel, wordpanel;
    CardLayout  bpanelcardlayout;
    JButton[]   letterbuttons, wordbuttons;
    int         screenwidth, screenheight, gamewidth, gameheight, gameposx, gameposy, fontsize, msgsize, bordersize, points;
    Border      pinkborder, whiteBorder, compondborder;
    String      msgtext;
    Writer      writer;

    public static void main(String[]args) 
    {       
        //New countdown game
        countdown game = new countdown();

    }

    countdown(){

        guisetup();
        startgame();

    }

    public void guisetup(){

        screen              = new JFrame();
        mainpanel           = new JPanel(new GridLayout(2,1));
        letterpanel         = new JPanel(new GridLayout(2,1));
        bottompanel         = new JPanel(new CardLayout());
        bpanelcardlayout    = (CardLayout)(bottompanel.getLayout());
        buttonpanel         = new JPanel(new GridLayout(1,1));
        blankpanel          = new JPanel(new GridLayout(1,1));
        scorepanel          = new JPanel(new GridLayout(1,1));
        randompanel         = new JPanel(new GridLayout(1,9));
        wordpanel           = new JPanel(new GridLayout(1,9));

        letterbuttons       = new JButton[9];
        wordbuttons         = new JButton[9];

        screenwidth         = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().width;
        screenheight        = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height;
        gamewidth           = screenwidth/3;
        gameheight          = gamewidth/2;
        gameposx            = (screenheight/2)-(gameheight/2);
        gameposy            = (screenwidth/2)-(gamewidth/2)-(gameheight/2);

        if(gamewidth > 800){
            bordersize = 6;
            fontsize = 50;
            msgsize = 40;
        }else if (gamewidth <= 800 & gamewidth >600){
            bordersize = 4;
            fontsize = 40;
            msgsize = 30;
        }else if (gamewidth <= 600 & gamewidth >400){
            bordersize = 3;
            fontsize = 30;
            msgsize = 20;
        }else {
            bordersize = 2;
            fontsize = 20;
            msgsize = 15;
        }

        pinkborder          = new LineBorder(new Color(209,190,245), bordersize);
        whiteBorder         = new LineBorder(Color.WHITE, bordersize*2);
        compondborder       = BorderFactory.createCompoundBorder(pinkborder, whiteBorder);

        //Window setup stuff
        screen.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        screen.setTitle( "Countdown" );
        screen.setLocation(gameposy,gameposx);
        screen.setSize(gamewidth,gameheight);

    }

    public void startgame(){

        // Button factory generates custom buttons for us
        Buttonfactory btnf          = new Buttonfactory();
        btnf.letterfontsize         = fontsize;
        btnf.choicefontsize         = fontsize;
        btnf.bordersize             = bordersize;
        btnf.pinkborder             = pinkborder;
        btnf.compondborder          = compondborder;
        
        JButton vowelbutton         = btnf.newchoicebutton("Vowel");
        JButton constonantbutton    = btnf.newchoicebutton("Consonant");

        //Settings for panel while round is under way
        blankpanel.setBorder(compondborder);
        blankpanel.setBackground(new Color(0,47,225));
        
        //Set up a new buttoncounter, passing in this game instance and the letterbuttons
        buttoncounter bc = new buttoncounter(this, letterbuttons);

        vowelbutton.addActionListener(bc);
        constonantbutton.addActionListener(bc);

        //Setting up a wordmaker object and passing it the letters 
        wordmaker wm = new wordmaker(wordbuttons, letterbuttons);
        
        for(int i = 0; i < letterbuttons.length; i++)
        {

            //Letter buttons are for the random letters
            letterbuttons[i] = btnf.newtilebutton("Letter");
            letterbuttons[i].putClientProperty("OriginalPosition", i);
            letterbuttons[i].setEnabled(false);
            letterbuttons[i].addActionListener(wm);
            
            //Word buttons are for the letters that represent a word when the user makes a word
            wordbuttons[i] = btnf.newtilebutton("Word");
            wordbuttons[i].setEnabled(false);  
            wordbuttons[i].addActionListener(wm);

            randompanel.add(letterbuttons[i]);
            wordpanel.add(wordbuttons[i]);

        }

        randompanel.setBackground(new Color(209,190,245));
        wordpanel.setBackground(new Color(209,190,245));
        letterpanel.setBackground(new Color(209,190,245));
        buttonpanel.setBackground(new Color(209,190,245));
        mainpanel.setBackground(new Color(209,190,245));
        screen.setBackground(new Color(209,190,245));

        //Settings up the panels for the cardlayout
        bottompanel.add("BUTTONPANEL", buttonpanel);    
        bottompanel.add("BLANKPANEL", blankpanel);
        bottompanel.add("SCOREPANEL", scorepanel);

        //Adding all the panels
        mainpanel.add(letterpanel);
        mainpanel.add(bottompanel);
        
        letterpanel.add(randompanel);
        letterpanel.add(wordpanel);
        
        buttonpanel.add(vowelbutton);
        buttonpanel.add(constonantbutton);  

        screen.add(mainpanel);
        
        screen.setVisible(true);
    }

    public void startround(){

        clock down = new clock(this);
        bpanelcardlayout.show(bottompanel, "BLANKPANEL");

    }

    public void endofround(){

        String madeword = "";
        String nextletter;
        boolean isspace = false;

        //Disable all letter and word buttons and get the made word
        for(int i = 0; i < letterbuttons.length; i++)
        {

            letterbuttons[i].setEnabled(false);
            wordbuttons[i].setEnabled(false);

            //Stops making word at first space
            nextletter = (String)wordbuttons[i].getClientProperty("Letter");

            //Check if a space has been detected and set isspace to true, if so then dont added anymore letters
            if(!isspace){

                if(nextletter.equals("")){
                        isspace = true;
                }else{
                    madeword = madeword + nextletter;
                }
            }

        }

        //Console output
        System.out.print("Made word: "+madeword+"\n");
        boolean wordexists = tools.IsInDictionary(madeword);
        System.out.print("Is word in the dictionary? "+ wordexists);

        //Settings for end of round panel
        scorepanel.setBorder(compondborder);
        scorepanel.setBackground(new Color(0,47,225));

        if(wordexists){
            points = madeword.length();
            msgtext = "<html><p align=center>Congratulations!<br> "+ madeword +" is in the dictionary!<br> " + points + " points</p></html>";
        }else{
            msgtext = "Wrong! "+ madeword +" is not a real word!";
            points = 0;
        }

        JLabel msg = new JLabel(msgtext, JLabel.CENTER);
        msg.setFont(new Font("sansserif",Font.BOLD, msgsize));
        msg.setForeground(Color.white);
        scorepanel.add(msg);

        //Show the score panel
        bpanelcardlayout.show(bottompanel, "SCOREPANEL");

        //Write score to file
        try {
            writer = new PrintWriter(new FileOutputStream(new File("scores.txt"), true)); 
            writer.write(madeword+" : "+ points +" points\r\n");
        } catch (IOException ex) {
        } finally {
           try {writer.close();} catch (Exception ex) {}
        }

    }

}

class tools{

    public static int findnextspace(boolean[] searchin){    
        //Searchs through the array and returns the index of first empty / falase position

        int i = 0;

        for( ; i < 9; i++){
            if (searchin[i] == false){
                break;
            }
        }   

        return i;
    }

    public static boolean IsInDictionary(String wordtocheck){

        //Remove any spaces
        wordtocheck = wordtocheck.replaceAll("\\s+","");
        //Make word all lowercase
        wordtocheck = wordtocheck.toLowerCase();

        Set<String> dictionaryset = new HashSet<String>();
        
        try{
            BufferedReader br =  new BufferedReader(new InputStreamReader(tools.class.getClassLoader().getResourceAsStream("dictionary.txt")));
            String line = br.readLine(); 
            while (line != null)  
            {  
             
             dictionaryset.add(line);
             line = br.readLine();

            }

        }catch(Exception errorrrrr){

        }

        boolean wordexists = dictionaryset.contains(wordtocheck);

        return wordexists;

    }

    public static String generateWeightedLetter(String type){

        Random randomer = new Random();
        String letter = "";
        String vowels = "AEIOU";
        String consonants = "BCDFGHJKLMNPQRSTVWXYZ";
        int position = -1;

        TreeMap<Integer, String> lf = new TreeMap<Integer, String>();

        lf.put(7, "Z");
        lf.put(17, "J");
        lf.put(28, "Q");
        lf.put(45, "X");
        lf.put(114, "K");
        lf.put(225, "V");
        lf.put(374, "B");
        lf.put(556, "P");
        lf.put(759, "G");
        lf.put(968, "W");
        lf.put(1179, "Y");
        lf.put(1409, "F");
        lf.put(1670, "M");
        lf.put(1941, "C");
        lf.put(2229, "U");
        lf.put(2627, "L");
        lf.put(3059, "D");
        lf.put(3651, "H");
        lf.put(4253, "R");
        lf.put(4881, "S");
        lf.put(5576, "N");
        lf.put(6307, "I");
        lf.put(7075, "O");
        lf.put(7887, "A");
        lf.put(8797, "T");
        lf.put(9999, "E");


        if("Consonant".equals(type)){

            while(position == -1){
                position = consonants.indexOf(lf.get(lf.higherKey(randomer.nextInt(9999))));
            }

            letter = Character.toString(consonants.charAt(position));       

        }

        if("Vowel".equals(type)){

            while(position == -1){
                position = vowels.indexOf(lf.get(lf.higherKey(randomer.nextInt(9999))));
            }

            letter = Character.toString(vowels.charAt(position));   

        }

        return letter; 

    }

}

class wordmaker implements ActionListener{

    JButton[] wordbuttons;
    JButton[] letterbuttons;
    boolean[] letterspaces = new boolean[9];//Keeps track of avaible spaces for the tiles

    wordmaker(JButton[] wordbuttons, JButton[] letterbuttons){
        this.wordbuttons = wordbuttons;
        this.letterbuttons = letterbuttons;
    }

    public void actionPerformed(ActionEvent e)
    {
        JButton button          = (JButton) e.getSource();
        //Get button type (Either random letter top row or word letter second row)
        String type             = (String)button.getClientProperty("Type"); 
        //Get the letter on the tile
        String letter           = (String)button.getClientProperty("Letter");   
        //Get letters original position in the top row
        int letterindex         = (Integer)button.getClientProperty("OriginalPosition");

        int nextspace;
        int currentindex;

        //Make clicked button dissapear
        button.setBackground(new Color(253,222,165));
        button.setEnabled(false);
        button.setText("");

        if(type == "Letter"){

            //Find first free space for it to go in on the word row
            nextspace = tools.findnextspace(letterspaces);

            //Mark that position as now taken
            letterspaces[nextspace] = true;

            //Set up the letter in the word row
            wordbuttons[nextspace].setText(letter);
            wordbuttons[nextspace].putClientProperty("CurrentPosition", nextspace);         
            wordbuttons[nextspace].putClientProperty("OriginalPosition", letterindex);
            wordbuttons[nextspace].putClientProperty("Letter", letter);
            wordbuttons[nextspace].setEnabled(true);
            wordbuttons[nextspace].setBackground(new Color(0,47,225));

        }

        if(type == "Word"){

            //Get letters current position in bottom row
            currentindex = (Integer)button.getClientProperty("CurrentPosition");
            button.putClientProperty("Letter", "");

            //Put the letter back into the top row
            letterbuttons[letterindex].setText(letter);
            letterbuttons[letterindex].setBackground(new Color(0,47,225));
            letterbuttons[letterindex].setEnabled(true);

            //Mark the current space as now empty
            letterspaces[currentindex] = false;

        }

    }

}

class buttoncounter implements ActionListener 
{

    JButton[] letterbuttons;
    int totalclicks = 0;
    countdown game;
    String letter;

    buttoncounter(countdown game, JButton[] letterbuttons){

        this.game = game;
        this.letterbuttons = letterbuttons;

    }

    public void actionPerformed(ActionEvent e)
    {

        totalclicks++;

        if(totalclicks == 9)
        {
            game.startround();
        }

        //Get either a vowel or consonant depending on what button was clicked
        if(e.getActionCommand() == "Vowel"){
            letter = tools.generateWeightedLetter("Vowel");
        }else{
            letter = tools.generateWeightedLetter("Consonant");
        }

        //Put letter tile into top row
        letterbuttons[totalclicks-1].setBackground(new Color(0,47,225));
        letterbuttons[totalclicks-1].setText(letter);
        letterbuttons[totalclicks-1].putClientProperty("Letter", letter);
        letterbuttons[totalclicks-1].setEnabled(true);  
        
    }
    
}

class Buttonfactory{

    int choicefontsize, letterfontsize, bordersize;
    Border pinkborder, compondborder;

    public JButton newtilebutton(String type){

            JButton button = new JButton();
            button.putClientProperty("Type", type);
            //Default to no letter
            button.putClientProperty("Letter", "");
            button.setFont(new Font("sansserif",Font.BOLD, letterfontsize));
            button.setBackground(new Color(253,222,165));
            button.setBorder(pinkborder);
            button.setHorizontalTextPosition(JButton.CENTER);
            button.setVerticalTextPosition(JButton.CENTER);
            button.setForeground(Color.WHITE);

            return button;
    }

    public JButton newchoicebutton(String text){

        JButton button = new JButton(text);
        button.setFont(new Font("sansserif",Font.BOLD, choicefontsize));
        button.setBackground(new Color(0,47,225));
        button.setBorder(compondborder);
        button.setHorizontalTextPosition(JButton.CENTER);
        button.setVerticalTextPosition(JButton.CENTER);
        button.setForeground(Color.WHITE);

        return button;
    }
}