import java.awt.* ; 
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import sun.audio.*;
import java.util.Calendar;
import java.awt.geom.*;

class clock
{   

    countdown currentgame;
    JFrame clockscreen = new JFrame();
    JPanel clockpanel = new JPanel(new GridLayout(1,1));
    Theclock clock ;
    

    clock (countdown game)
    {   
        this.currentgame = game;
        int gameheight = game.screen.getHeight();
        int gamewidth = game.screen.getWidth();
        Point gamepos = game.screen.getLocationOnScreen();
        clock = new Theclock(currentgame);
        clockscreen.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        clockscreen.setTitle( "Countdown" );
        clockscreen.setLocation(gamepos.x+gamewidth,gamepos.y);      
        clockscreen.setSize(gameheight, gameheight); 
        clockscreen.setVisible(true);
        clockscreen.add(clockpanel);
        clockpanel.add(clock);

        clock.StartTheCountdown();
    }

}

class Theclock extends JPanel implements ActionListener
{

    Timer clockticker = new Timer(40,this);
    long starttime;
    double theta = 0;
    countdown currentgame;

    Theclock(countdown game){
        currentgame = game;
        setBackground(new Color(209,190,245));
    }

    public void StartTheCountdown(){

        //Prevents lag when resizing by colapasing all events
        clockticker.setCoalesce(false);
        starttime = System.nanoTime();
        System.out.println(starttime);
        clockticker.start();
        StarttheMusic();
    }

    public void StopTheCountdown(){
        clockticker.stop();
        currentgame.endofround();
    }

    public void StarttheMusic(){
        // open the sound file as a Java input stream
        String gongFile = "theme.wav";
        try{

            InputStream in = getClass().getResourceAsStream(gongFile);          
            // create an audiostream from the inputstream
            AudioStream audioStream = new AudioStream(in);
            // play the audio clip with the audioplayer class
            AudioPlayer.player.start(audioStream);

        }catch(Exception errorrrrrrrrr){

        }
    }

    public void paintComponent(Graphics gc)
    {
        super.paintComponent(gc);
        double x = getWidth();
        double y = getHeight();

        //Divide by 800 as this clock was designed around having a height and width of 800
        double xratio = x/800;
        double yratio = y/800;

        Graphics2D g2d = (Graphics2D)gc;
        //Improves Quality
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        AffineTransform tx = new AffineTransform();
        tx.scale(xratio,yratio);
        g2d.setTransform(tx);       

        //Clock frame
        g2d.setColor(new Color(159,131,154));       
        g2d.fillOval(0,0,800,800);
        //Non-Lite Clock background
        g2d.setColor(new Color(237,180,186));       
        g2d.fillOval(20,20,760,760);

        Arc2D arc = null;
        g2d.setColor(new Color(255,238,208));
        arc = new Arc2D.Double(20, 20, 760, 760, 90, -theta, Arc2D.PIE);
        g2d.fill(arc);

        //Non-Lite Clock center background
        g2d.setColor(new Color(237,180,186));       
        g2d.fillOval(240,240,320,320);

        //Crosshairs
        g2d.setColor(new Color(159,131,154));   

        g2d.fillRect(0,398,800,4);
        g2d.fillRect(398,0,4,800);
        tx.rotate(Math.PI/6, 400, 400);
        g2d.setTransform(tx);
        g2d.fillRoundRect(398,40,4,100,15,15);
        tx.rotate(Math.PI/6, 400, 400);
        g2d.setTransform(tx);
        g2d.fillRoundRect(398,40,4,100,15,15);
        tx.rotate(Math.PI/3, 400, 400);
        g2d.setTransform(tx);
        g2d.fillRoundRect(398,40,4,100,15,15);
        tx.rotate(Math.PI/6, 400, 400);
        g2d.setTransform(tx);
        g2d.fillRoundRect(398,40,4,100,15,15);
        tx.rotate(Math.PI/3, 400, 400);
        g2d.setTransform(tx);
        g2d.fillRoundRect(398,40,4,100,15,15);
        tx.rotate(Math.PI/6, 400, 400);
        g2d.setTransform(tx);
        g2d.fillRoundRect(398,40,4,100,15,15);
        tx.rotate(Math.PI/3, 400, 400);
        g2d.setTransform(tx);
        g2d.fillRoundRect(398,40,4,100,15,15);
        tx.rotate(Math.PI/6, 400, 400);
        g2d.setTransform(tx);
        g2d.fillRoundRect(398,40,4,100,15,15);

        //Cicrlce Border
        g2d.setColor(new Color(159,131,154));   
        Shape circle = new Ellipse2D.Float(350,350,100,100);
        g2d.fill(circle);

        //Cicrlce Inside
        gc.setColor(new Color(0,47,225));       
        gc.fillOval(370,370,60,60);

        //Rotate back to start
        tx.rotate(Math.PI/6, 400, 400);
        tx.rotate(Math.toRadians(theta), 400, 400);
        g2d.setTransform(tx);
        //Arrow border
        g2d.setColor(new Color(159,131,154));
        Polygon p = new Polygon();
        p.addPoint(350,400);
        p.addPoint(400,200);
        p.addPoint(450,400);
        g2d.fill(p);
        //Arrow Inside
        g2d.setColor(new Color(0,47,225));
        Polygon p2 = new Polygon();
        p2.addPoint(370,400);
        p2.addPoint(400,220);
        p2.addPoint(430,400);
        g2d.fill(p2);

    }

    public void actionPerformed(ActionEvent e)
    {
        
        this.repaint();

        //Theta worked out based on what it should be front the elapsed time
        theta = ((System.nanoTime() - starttime)  * 0.000000006);

        if((int)theta == 180){
            StopTheCountdown();
        }
    }

}